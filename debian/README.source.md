# Introduction

This package or, more accurately, collection of packages is meant to provide a
simple and repeatable way to provide the latest Firefox ESR (from an upstream
Mozilla build for Linux amd64), and one or more associated extensions, all in
`.deb` packages for ease of deployment onto older Debian systems for which the
latest Firefox ESR might not be available. Generally, the unavailability of the
latest Firefox ESR for older Debian releases stems from the inability of
maintainers to support Firefox's extremely complex toolchain on older Debian
releases, preventing the backporting of the `firefox-esr` source package from
current Debian releases.

The initial need for this package was driven by the situation on Debian stretch,
where the latest available version based on Debian packages built from source is
91.11.0esr. The packages built from this source package are thus targeted at
Debian stretch. The installation and upgrades have been tested on Debian
stretch, but there is no specific reason that the binary packages should not
also be installable on other releases that meet the minimum requirements for
running the upstream binary builds from Mozilla.

## How This Collection of Packages is Intended to Function

This package does not actually perform any compilation or linking, but rather
takes already built artifacts and repackages them for deployment. Because in the
case of this package the traditional "upstream" part of the package is not
present in a source form but rather as binary artifacts, there are no upstream
tarballs to manage nor are the multiple branches typically associated with
`git-buildpackage` (`pristine-tar`, `upstream`, and `master`) present in this
repository. All changes for this package, both upstream and Debian-specific, are
made on the `main` branch. This requires special care and this `README` should
be read completely prior to making any changes to this repository.

Note that the `git-buildpackage` documentation contains a chapter __When
upstream uses Git__, which itself contains a section __No upstream tarballs__.
The reader is strongly encouraged to review [that
portion](https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.import.upstream-git.html)
of the documentation prior to proceeding.

## Upstream

The upstream components that make up this package collection are all contained
in the `components` subdirectory of this repository. As a result, updating the
upstream components requires manual work (i.e., `gbp import-*` will not work).

### Component Updates

Updating the upstream components involves several steps.

- Download the latest Firefox ESR (en-US) release, including the tarball and
  detached signature file. Example URL:
  <https://releases.mozilla.org/pub/firefox/releases/102.4.0esr/linux-x86_64/en-US/>
- Download the French language pack (filename: `fr.xpi`) associated with that
  same release into the `components/lang` directory. Example URL:
  <https://releases.mozilla.org/pub/firefox/releases/102.4.0esr/linux-x86_64/xpi/>
- Calculate the checksum for the language pack: `sha256sum fr.xpi >
  fr.xpi.sha256`
- Download the latest uBlock Origin extension. URL:
  <https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/>
- Calculate the checksum for the extension: `sha256sum ublock_origin-1.45.0.xpi >
  ublock_origin-1.45.0.xpi.sha256`
- Note: if additional language packs or extensions are required, consult the
  next section
- Remove all the files under the `components/` subdirectory: `git rm -r
  components/*`
- Move the newly downloaded files (and their signature files or checksum files,
  as applicable) into the `components/` subdirectory (`components/lang` for
  language packs).
- Confirm that the new file names match the patterns of the old filenames, with
  the only differences being version numbers (except for language packs, which
  will overwrite their predecessors).
- Stage all the files under the `components/` subdirectory: `git add
  components/*`
- You can run `./debian/rules checksigs` to verify signatures and checksums.
- Commit the staged files. Examine the git history to see examples of commit
  messages for upstream component updates.

### Adding Components (Language Packs or Extensions)

If additional language packs are required, they should also be downloaded into
`components/lang` and their checksums calculated. If new language packs are
added, add a new binary package in `debian/control`, place an appropriate
`.install` file in `debian/`,  and also update `debian/rules` following the
pattern of existing language packs. Make sure that a new Firefox ESR release is
accompanied by all new language packs.

Additional information on deploying language packs:
<https://support.mozilla.org/en-US/kb/deploying-firefox-language-packs>

Additional extensions are little more involved. It is necessary to download the
extension and then unpack it to identify its "add-on ID". The "add-on ID" is
used in constructing the installation target in `debian/rules` and in the
`.install` file for the binary package which will contain the extension.

Additional information on deploying extensions:
<https://support.mozilla.org/en-US/kb/deploying-firefox-with-extensions>

When adding new language packs or extensions, update this README so that
maintainers performing future updates have a complete list of items to download.
Also, update `debian/copyright` with a link to the license for the extension.

### Tagging

The upstream version must be tagged properly to ensure that `git-buildpackage`
is able to properly generate the upstream tarball and then build the Debian
packages. The upstream version of this package is based on the included Firefox
ESR version. So, if the upstream components have been updated to include
version 102.4.0esr then that needs to be the version tagged to the commit which
represents the updated upstream components. For example:

```
git tag 102.4.0esr
```

Once the upstream components are updated and the upstream tag is made, then the
Debian-specific packaging work can proceed.

## Debian Packaging

Apart from the unique structure of the upstream components most of the
Debian-specific parts of the workflow will be very familiar. One particular
item warranting special attention is that particular care should be taken to not
make changes outside of the `debian/` subdirectory unless those changes are
specifically part of the previously described workflow for updating the upstream
components.

Ensure that the `.install` files in the `debian/` subdirectory for each
extension are updated to reflect the new file name in the `components/`
subdirectory. Specifically, this means updating the version string in the
source file name.

One other item to note is that not all the binary packages will have the same
version number. This is because the extensions (currently only uBlock Origin)
will have different version numbers from the main Firefox package. This means
that extensions cannot be updated without a corresponding update to the primary
Firefox ESR component. The calculation of any additional version numbers will be
handled by `debian/rules`.

### Building/Verifying Packages

Once the upstream components are updated, create a new entry in
`debian/changelog`. Take note of prior entries and make sure that the new entry
both reflects the correct upstream version (else `git-buildpackage` will fail to
create the upstream tarball) and follows the special pattern for the Debian part
of the version number. Because no compilation or linking takes place, building
within a chroot or pbuilder is not strictly necessary.

Check to ensure that GPG key in `debian/mozilla_key.asc` is valid (i.e., not
expired) and that it successfully validates the detached signature associated
with the Firefox release being handled. For example:

```
$ gpg --import debian/mozilla_key.asc
gpg: key 61B7B526D98F0353: 24 signatures not checked due to missing keys
gpg: key 61B7B526D98F0353: public key "Mozilla Software Releases <release@mozilla.com>" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: no ultimately trusted keys found
$ gpg --verify components/firefox-102.4.0esr.tar.bz2.asc 
gpg: assuming signed data in 'components/firefox-102.4.0esr.tar.bz2'
gpg: Signature made Mon Oct 10 13:17:13 2022 UTC
gpg:                using RSA key 4360FE2109C49763186F8E21EBE41E90F6F12F6D
gpg: Good signature from "Mozilla Software Releases <release@mozilla.com>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 14F2 6682 D091 6CDD 81E3  7B6D 61B7 B526 D98F 0353
     Subkey fingerprint: 4360 FE21 09C4 9763 186F  8E21 EBE4 1E90 F6F1 2F6D
```

If the check fails, update the key as part of preparing the next Debian package
release. The key used to sign the Firefox release tarball can be found in a
parent directory of the location where the tarball was found. Example URL:
<https://releases.mozilla.org/pub/firefox/releases/102.4.0esr/>

Sensible options are configured in `debian/gbp.conf` such that a simple
invocation of `gbp buildpackage` should produce the expected packages.

### Tagging

The tagging facility of `git-buildpackage` is appropriate and sufficient for
this repository:

```
gbp buildpackage --git-tag --git-tag-only
```
